﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BirdMovement : MonoBehaviour {

    float flapSpeed    = 4f;
    float forwardSpeed = 1f;

    bool didFlap = false;
    public bool dead = false;
    float deathCooldown;

    Animator anim;

	// Use this for initialization
	void Start () {
        anim = transform.GetComponentInChildren<Animator>();
	}

    //Do graphic & input update
    void Update()
    {
        if (dead)
        {
            deathCooldown -= Time.deltaTime;

            if(deathCooldown <= 0)
            {
                if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
                    SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
            {
                didFlap = true;
            }
        }
    }
	
	// Do physics engine update
	void FixedUpdate () {
        if (dead)
        {
            return;
        }

        this.GetComponent<Rigidbody2D>().AddForce(Vector2.right * forwardSpeed);

        if (didFlap)
        {
            this.GetComponent<Rigidbody2D>().AddForce(Vector2.up * flapSpeed, ForceMode2D.Impulse);
            anim.SetTrigger("DoFlap");
            didFlap = false;
        }

        float angle = 0;
        if(this.GetComponent<Rigidbody2D>().velocity.y < 0)
        {
            angle = Mathf.Lerp(0, -90, -this.GetComponent<Rigidbody2D>().velocity.y / 5f);
        }
        transform.rotation = Quaternion.Euler(0, 0, angle);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        dead = true;
        anim.SetTrigger("Death");
        deathCooldown = 0.5f;
    }
}
