﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    static int score = 0;
    static int highScore = 0;
    Text text;
    BirdMovement bird;

    static Score instance;

    static public void AddPoint()
    {
        if (instance.bird.dead)
            return;

        score++;
        if (score > highScore)
            highScore = score;
    }

    void Start()
    {
        instance = this;

        GameObject player_go = GameObject.FindGameObjectWithTag("Player");
        if (player_go == null)
        {
            Debug.LogError("Could not find a 'Player'!");
            return;
        }
        bird = player_go.GetComponent<BirdMovement>();

        text = GetComponent<Text>();
        score = 0;
        highScore = PlayerPrefs.GetInt("highScore", 0);
    }

    void OnDestroy()
    {
        instance = null;
        PlayerPrefs.SetInt("highScore", highScore);
    }
	
	// Update is called once per frame
	void Update () {
        text.text = "SCORE: " + score + "\nHIGHSCORE: " + highScore;
	}
}
